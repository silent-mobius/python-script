
---
# Python Script Arrays

.footer: Created by Alex M. Schapelle, VaioLabs.IO

---
## Arrays as an idea

An array is structure: a form in which we can keep information for later use. That aside, array has very specific description: `An array is a collection of same type of elements which are sheltered under a common name.`for  `c` programming language array:

```c
 int array1={1,2,3,4,5}
```

Yet, when it comes to Python language, there are no `Arrays` per-se. Instead we'll be working with other ways, also known as data-structures, to store information. Although there is array library added in python3, we will `not` be working with it.


---
# Python Script Arrays
## Lists
Lists are used to store multiple items in a single variable.
Lists are one of 4 built-in data types in Python used to store collections of data, the other 3 are Tuple, Set, and Dictionary, all with different qualities and usage.
Lists are created using square brackets:

```py
thislist = ["tomato", "cabbage", "cucumber"]
print(thislist) 
```

---

# Python Script Arrays
## List Items

List items are ordered, changeable, and allow duplicate values.

List items are indexed, the first item has index [0], the second item has index [1] etc.

---

# Python Script Arrays
## Ordered

When we say that lists are ordered, it means that the items have a defined order, and that order will not change.

If you add new items to a list, the new items will be placed at the end of the list.

> `[!]`Note: There are some list methods that will change the order, but in general: the order of the items will not change.

---
# Python Script Arrays
## Changeable

The list is changeable, meaning that we can change, add, and remove items in a list after it has been created.

---

# Python Script Arrays
## Allow Duplicates

Since lists are indexed, lists can have items with the same value:
Lists allow duplicate values:
```py
thislist = ["tomato", "cabbage", "cucumber", "tomato", "lettuce"]
print(thislist) 
```
---
# Python Script Arrays
## List Length

To determine how many items a list has, use the len() function:

Print the number of items in the list:
```py
thislist = ["tomato", "cabbage", "cucumber"]
print(len(thislist)) 
```
---
## List Items - Data Types

List items can be of any data type:
String, int and boolean data types:
```py
list1 = ["tomato", "cabbage", "cucumber"]
list2 = [1, 5, 7, 9, 3]
list3 = [True, False, False] 
```
A list can contain different data types:
A list with strings, integers and boolean values:

```py
list1 = ["abc", 34, True, 40, "male"] 
```
---

# Python Script Arrays
## `type()` Function

From Python's perspective, lists are defined as objects with the data type 'list':
<class 'list'>

What is the data type of a list?
```py
mylist = ["tomato", "cabbage", "cucumber"]
print(type(mylist)) 
```
---
# Python Script Arrays
## The `list()` Constructor

It is also possible to use the list() constructor when creating a new list.

Using the list() constructor to make a List:
```py
thislist = list(("tomato", "cabbage", "cucumber")) # note the double round-brackets
print(thislist) 
```
---
# Python Script Arrays
## Access Items

List items are indexed and you can access them by referring to the index number:
```py
Print the second item of the list:
thislist = ["tomato", "cabbage", "cucumber"]
print(thislist[1])
```
> `[!]`Note: The first item has index 0.


---

# Python Script Arrays
## Negative Indexing

Negative indexing means start from the end

-1 refers to the last item, -2 refers to the second last item etc.
```py
Print the last item of the list:
thislist = ["tomato", "cabbage", "cucumber"]
print(thislist[-1])
```
---

# Python Script Arrays
## Range of Indexes

You can specify a range of indexes by specifying where to start and where to end the range.

When specifying a range, the return value will be a new list with the specified items.
Return the third, fourth, and fifth item:

```py
thislist = ["tomato", "cabbage", "cucumber", "mushroom", "lettuce", "celery", "carrot"]
print(thislist[2:5])
```
> `[!]`Note: The search will start at index 2 (included) and end at index 5 (not included).
> `[!]`Note: Remember that the first item has index 0.
---

# Python Script Arrays
## Range of Indexes (cont.)


By leaving out the start value, the range will start at the first item:
This  returns the items from the beginning to, but NOT including, "lettuce":
```py
thislist = ["tomato", "cabbage", "cucumber", "mushroom", "lettuce", "celery", "carrot"]
print(thislist[:4])
```
---

# Python Script Arrays
## Range of Indexes (cont.)


By leaving out the end value, the range will go on to the end of the list:
This  returns the items from "cucumber" to the end:
```py
thislist = ["tomato", "cabbage", "cucumber", "mushroom", "lettuce", "celery", "carrot"]
print(thislist[2:])
```
---
# Python Script Arrays
## Range of Negative Indexes

Specify negative indexes if you want to start the search from the end of the list:
This  returns the items from "mushroom" (-4) to, but NOT including "carrot" (-1):
```py
thislist = ["tomato", "cabbage", "cucumber", "mushroom", "lettuce", "celery", "carrot"]
print(thislist[-4:-1])
```
---
# Python Script Arrays
## Check if Item Exists

To determine if a specified item is present in a list use the in keyword:
Check if "tomato" is present in the list:

```py
thislist = ["tomato", "cabbage", "cucumber"]
if "tomato" in thislist:
  print("Yes, 'tomato' is in the fruits list") 
```
---
# Python Script Arrays
## Change Item Value

To change the value of a specific item, refer to the index number:

Change the second item:

```py
thislist = ["tomato", "cabbage", "cucumber"]
thislist[1] = "onion"
print(thislist)
```


---

# Python Script Arrays

## Change a Range of Item Values

To change the value of items within a specific range, define a list with the new values, and refer to the range of index numbers where you want to insert the new values:

Change the values "cabbage" and "lettuce" with the values "onion" and "nori":
```py
thislist = ["tomato", "cabbage", "cucumber", "mushroom", "lettuce", "celery", "carrot"]
thislist[1:3] = ["onion", "nori"]
print(thislist)
```

---
# Python Script Arrays

## Change a Range of Item Values (cont.)

If you insert more items than you replace, the new items will be inserted where you specified, and the remaining items will move accordingly:

Change the second value by replacing it with two new values:
```py
thislist = ["tomato", "cabbage", "cucumber"]
thislist[1:2] = ["onion", "nori"]
print(thislist)
```
> `[!]`Note: The length of the list will change when the number of items inserted does not match the number of items replaced.

If you insert less items than you replace, the new items will be inserted where you specified, and the remaining items will move accordingly:

---

# Python Script Arrays

## Change a Range of Item Values (cont.)
Change the second and third value by replacing it with one value:
```py
thislist = ["tomato", "cabbage", "cucumber"]
thislist[1:3] = ["nori"]
print(thislist)
```

---
# Python Script Arrays

## Insert Items

To insert a new list item, without replacing any of the existing values, we can use the insert() method.

The `insert()` method inserts an item at the specified index:

Insert "nori" as the third item:
```py
thislist = ["tomato", "cabbage", "cucumber"]
thislist.insert(2, "nori")
print(thislist)
```

> `[!]`Note: As a result of the  above, the list will now contain 4 items.

---
# Append Items

To add an item to the end of the list, use the `append()` method:


Using the append() method to append an item:
```py
thislist = ["tomato", "cabbage", "cucumber"]
thislist.append("lemongrass")
print(thislist)
```

# Insert Items

To insert a list item at a specified index, use the `insert() `method.

The insert() method inserts an item at the specified index:


Insert an item as the second position:
```py

thislist = ["tomato", "cabbage", "cucumber"]
thislist.insert(1, "lemongrassgrass")
print(thislist)
```

> `[!]`Note: As a result of the s above, the lists will now contain 4 items.

---

# Extend List

To append elements from another list to the current list, use the `extend()` method.


Add the elements of tropical to thislist:
```py
thislist = ["tomato", "cabbage", "cucumber"]
otherlist = ["wasabi", "zucchini", "papaya"]
thislist.extend(otherlist)
print(thislist)
```

The elements will be added to the end of the list.
---

# Add Any Iterable

The extend() method does not have to append lists, you can add any iterable object (tuples, sets, dictionaries etc.).

Add elements of a tuple to a list:
```py
thislist = ["tomato", "cabbage", "cucumber"]
thistuple = ("potato", "lemongrass")
thislist.extend(thistuple)
print(thislist)
```

---

# Remove Specified Item

The remove() method removes the specified item.


Remove "cabbage":
```pybanana
thislist = ["tomato", "cabbage", "cucumber"]
thislist.remove("cabbage")
print(thislist)
```

---

# Remove Specified Index

The pop() method removes the specified index.

Remove the second item:
```py
thislist = ["tomato", "cabbage", "cucumber"]
thislist.pop(1)
print(thislist)
```

If you do not specify the index, the pop() method removes the last item.

Remove the last item:
```py
thislist = ["tomato", "cabbage", "cucumber"]
thislist.pop()
print(thislist)
```

---
# The del keyword 

## also removes the specified index:

Remove the first item:
```py
thislist = ["tomato", "cabbage", "cucumber"]
del thislist[0]
print(thislist)
```

---

# The del keyword

## can also delete the list completely.

Delete the entire list:
```py
thislist = ["tomato", "cabbage", "cucumber"]
del thislist
```

---

# Clear the List

The clear() method empties the list.

The list still remains, but it has no content.

Clear the list content:
```py
thislist = ["tomato", "cabbage", "cucumber"]
thislist.clear()
print(thislist) 
```

---

# Sort List Alphanumerically

List objects have a sort() method that will sort the list alphanumerically, ascending, by default:


Sort the list alphabetically:
```py
thislist = ["lemongrass", "wasabi", "potato", "zucchini", "cabbage"]
thislist.sort()
print(thislist)
```

Sort the list numerically:
```py

thislist = [100, 50, 65, 82, 23]
thislist.sort()
print(thislist)
```

---

# Sort Descending

To sort descending, use the keyword argument reverse = True:


Sort the list descending:
```py

thislist = ["lemongrass", "wasabi", "potato", "zucchini", "cabbage"]
thislist.sort(reverse = True)
print(thislist)
```
Sort the list descending:

```py
thislist = [100, 50, 65, 82, 23]
thislist.sort(reverse = True)
print(thislist)
```

---

# Case Insensitive Sort

By default the sort() method is case sensitive, resulting in all capital letters being sorted before lower case letters:


Case sensitive sorting can give an unexpected result:
```py

thislist = ["cabbage", "lemongrass", "potato", "lettuce"]
thislist.sort()
print(thislist)
```

Luckily we can use built-in functions as key functions when sorting a list.

So if you want a case-insensitive sort function, use str.lower as a key function:

Perform a case-insensitive sort of the list:
```py

thislist = ["cabbage", "lemongrass", "potato", "lettuce"]
thislist.sort(key = str.lower)
print(thislist)
```
---

# Reverse Order

What if you want to reverse the order of a list, regardless of the alphabet?

The reverse() method reverses the current sorting order of the elements.

Reverse the order of the list items:

```py
thislist = ["cabbage", "lemongrass", "potato", "lettuce"]
thislist.reverse()
print(thislist)
```

---
# Copy a List

You cannot copy a list simply by typing list2 = list1, because: list2 will only be a reference to list1, and changes made in list1 will automatically also be made in list2.

There are ways to make a copy, one way is to use the built-in List method copy().


Make a copy of a list with the copy() method:
```py

thislist = ["tomato", "cabbage", "cucumber"]
mylist = thislist.copy()
print(mylist)
```

---

# Copy a List

Another way to make a copy is to use the built-in method list().


Make a copy of a list with the list() method:
```py

thislist = ["tomato", "cabbage", "cucumber"]
mylist = list(thislist)
print(mylist)
```

---


# Tuples

Tuples are used to store multiple items in a single variable.

Tuple is one of 4 built-in data types in Python used to store collections of data, the other 3 are List, Set, and Dictionary, all with different qualities and usage.

A tuple is a collection which is ordered and unchangeable.

Tuples are written with round brackets.


Create a Tuple:
```py

thistuple = ("tomato", "cabbage", "cucumber")
print(thistuple)
```
---
# Practice

 - create python script:
    - has variable meal with type of list
    - require from user to insert his lunch parts, with loop.
    - insert lunch parts to meal list
    - insert main course of lunch as seperate list in to meal list
    - sort both list in reverse order
    - append to meal list dessert you wish you had.
    - print the meal list in loop
     

---

# Tuple Items

Tuple items are ordered, unchangeable, and allow duplicate values.

Tuple items are indexed, the first item has index [0], the second item has index [1] etc.
Ordered

When we say that tuples are ordered, it means that the items have a defined order, and that order will not change.
Unchangeable

Tuples are unchangeable, meaning that we cannot change, add or remove items after the tuple has been created.
Allow Duplicates

Since tuples are indexed, they can have items with the same value:


Tuples allow duplicate values:
```py
thistuple = ("tomato", "cabbage", "cucumber", "tomato", "lettuce")
print(thistuple)
```

---

# Tuple Length

To determine how many items a tuple has, use the len() function:


Print the number of items in the tuple:
```py
thistuple = ("tomato", "cabbage", "cucumber")
print(len(thistuple))
```

---

# Create Tuple With One Item

To create a tuple with only one item, you have to add a comma after the item, otherwise Python will not recognize it as a tuple.


One item tuple, remember the comma:
```py

thistuple = ("tomato",)
print(type(thistuple))
```
---
# NOT a tuple
```py
thistuple = ("tomato")
print(type(thistuple))
```

---

# Tuple Items - Data Types

Tuple items can be of any data type:


String, int and boolean data types:
```py
tuple1 = ("tomato", "cabbage", "cucumber")
tuple2 = (1, 5, 7, 9, 3)
tuple3 = (True, False, False)
```

---

A tuple can contain different data types:


A tuple with strings, integers and boolean values:
```py
tuple1 = ("abc", 34, True, 40, "male")
```
---

# type()

From Python's perspective, tuples are defined as objects with the data type 'tuple':
<class 'tuple'>


What is the data type of a tuple?
```py
mytuple = ("tomato", "cabbage", "cucumber")
print(type(mytuple))
```

---

# The tuple() Constructor

It is also possible to use the tuple() constructor to make a tuple.


Using the tuple() method to make a tuple:
```py
thistuple = tuple(("tomato", "cabbage", "cucumber")) # note the double round-brackets
print(thistuple)
```

---
# Access Tuple Items

You can access tuple items by referring to the index number, inside square brackets:


Print the second item in the tuple:
```py
thistuple = ("tomato", "cabbage", "cucumber")
print(thistuple[1])
```

> `[!]`Note: The first item has index 0.

---
# Negative Indexing

Negative indexing means start from the end.

-1 refers to the last item, -2 refers to the second last item etc.


Print the last item of the tuple:
```py
thistuple = ("tomato", "cabbage", "cucumber")
print(thistuple[-1])
```
---

# Range of Indexes

You can specify a range of indexes by specifying where to start and where to end the range.

When specifying a range, the return value will be a new tuple with the specified items.


Return the third, fourth, and fifth item:
```py
thistuple = ("tomato", "cabbage", "cucumber", "lemongrass", "potato", "soybean", "wasabi")
print(thistuple[2:5])
```

> `[!]`Note: The search will start at index 2 (included) and end at index 5 (not included).

> `[!]`Note: Remember that the first item has index 0.

---

By leaving out the start value, the range will start at the first item:


This  returns the items from the beginning to, but NOT included, "potato":
```py
thistuple = ("tomato", "cabbage", "cucumber", "lemongrass", "potato", "soybean", "wasabi")
print(thistuple[:4])
```
---

By leaving out the end value, the range will go on to the end of the list:


This  returns the items from "lettuce" and to the end:
```py
thistuple = ("tomato", "cabbage", "cucumber", "lemongrass", "potato", "soybean", "wasabi")
print(thistuple[2:])
```
---
# Range of Negative Indexes

Specify negative indexes if you want to start the search from the end of the tuple:


This  returns the items from index -4 (included) to index -1 (excluded)
```py
thistuple = ("tomato", "cabbage", "cucumber", "lemongrass", "potato", "soybean", "wasabi")
print(thistuple[-4:-1])
```
---

# Check if Item Exists

To determine if a specified item is present in a tuple use the in keyword:


Check if "tomato" is present in the tuple:
```py

thistuple = ("tomato", "cabbage", "cucumber")
if "tomato" in thistuple:
  print("Yes, 'tomato' is in the fruits tuple") 
```

---

# Change Tuple Values

Tuples are unchangeable, meaning that you cannot change, add, or remove items once the tuple is created.

Once a tuple is created, you cannot change its values. Tuples are unchangeable, or immutable as it also is called.

But there is a workaround. You can convert the tuple into a list, change the list, and convert the list back into a tuple.


Convert the tuple into a list to be able to change it:
```py
x = ("tomato", "cabbage", "cucumber")
y = list(x)
y[1] = "potato"
x = tuple(y)

print(x)
```
---

# Add Items

Since tuples are immutable, they do not have a build-in append() method, but there are other ways to add items to a tuple.

1. Convert into a list: Just like the workaround for changing a tuple, you can convert it into a list, add your item(s), and convert it back into a tuple.


Convert the tuple into a list, add "lemongrass", and convert it back into a tuple:
```py

thistuple = ("tomato", "cabbage", "cucumber")
y = list(thistuple)
y.append("lemongrass")
thistuple = tuple(y)
```
---

2. Add tuple to a tuple. You are allowed to add tuples to tuples, so if you want to add one item, (or many), create a new tuple with the item(s), and add it to the existing tuple:


Create a new tuple with the value "lemongrass", and add that tuple:
```py
thistuple = ("tomato", "cabbage", "cucumber")
y = ("lemongrass",)
thistuple += y

print(thistuple)
```
> `[!]`Note: When creating a tuple with only one item, remember to include a comma after the item, otherwise it will not be identified as a tuple.

---

# Remove Items

> `[!]`Note: You cannot remove items in a tuple.

Tuples are unchangeable, so you cannot remove items from it, but you can use the same workaround as we used for changing and adding tuple items:


Convert the tuple into a list, remove "tomato", and convert it back into a tuple:
```py
thistuple = ("tomato", "cabbage", "cucumber")
y = list(thistuple)
y.remove("tomato")
thistuple = tuple(y)
```

---

Or you can delete the tuple completely:


The del keyword can delete the tuple completely:
```py
thistuple = ("tomato", "cabbage", "cucumber")
del thistuple
print(thistuple) #this will raise an error because the tuple no longer exists 
```

---

# Unpacking a Tuple

When we create a tuple, we normally assign values to it. This is called "packing" a tuple:


Packing a tuple:
```py
fruits = ("tomato", "cabbage", "cucumber")
```
But, in Python, we are also allowed to extract the values back into variables. This is called "unpacking":


Unpacking a tuple:
```py
fruits = ("tomato", "cabbage", "cucumber")

(green, yellow, red) = fruits

print(green)
print(yellow)
print(red)
```

> `[!]`Note: The number of variables must match the number of values in the tuple, if not, you must use an asterisk to collect the remaining values as a list.

---

# Using Asterisk*

If the number of variables is less than the number of values, you can add an * to the variable name and the values will be assigned to the variable as a list:


Assign the rest of the values as a list called "red":
```py
fruits = ("tomato", "cabbage", "cucumber", "strawberry", "raspberry")

(green, yellow, *red) = fruits

print(green)
print(yellow)
print(red)
```

If the asterisk is added to another variable name than the last, Python will assign values to the variable until the number of values left matches the number of variables left.


Add a list of values the "tropic" variable:
```py
fruits = ("tomato", "wasabi", "papaya", "zucchini", "lettuce")

(green, *tropic, red) = fruits

print(green)
print(tropic)
print(red)
```

---

# Join Two Tuples

To join two or more tuples you can use the + operator:


Join two tuples:
```py
tuple1 = ("a", "b" , "c")
tuple2 = (1, 2, 3)

tuple3 = tuple1 + tuple2
print(tuple3)
```

---


# Multiply Tuples

If you want to multiply the content of a tuple a given number of times, you can use the * operator:


Multiply the fruits tuple by 2:
```py
fruits = ("tomato", "cabbage", "cucumber")
mytuple = fruits * 2

print(mytuple) 
```

---
# Sets

Sets are used to store multiple items in a single variable.

Set is one of 4 built-in data types in Python used to store collections of data, the other 3 are List, Tuple, and Dictionary, all with different qualities and usage.

A set is a collection which is unordered, unchangeable*, and unindexed.

> `[!]`Note: Set items are unchangeable, but you can remove items and add new items.

Sets are written with curly brackets.


Create a Set:
```py

thisset = {"tomato", "cabbage", "cucumber"}
print(thisset)
```

> `[!]`Note: Sets are unordered, so you cannot be sure in which order the items will appear.

---

# Set Items

Set items are unordered, unchangeable, and do not allow duplicate values.

Unordered means that the items in a set do not have a defined order.

Set items can appear in a different order every time you use them, and cannot be referred to by index or key.

Set items are unchangeable, meaning that we cannot change the items after the set has been created.

Once a set is created, you cannot change its items, but you can remove items and add new items.

---

# Duplicates Not Allowed

Sets cannot have two items with the same value.


Duplicate values will be ignored:
```py

thisset = {"tomato", "cabbage", "cucumber", "tomato"}

print(thisset)
```
---
# len()

Get the Length of a Set

To determine how many items a set has, use the len() method.


Get the number of items in a set:
```py

thisset = {"tomato", "cabbage", "cucumber"}

print(len(thisset))
```

---

# Set Items - Data Types

Set items can be of any data type:


String, int and boolean data types:
```py
set1 = {"tomato", "cabbage", "cucumber"}
set2 = {1, 5, 7, 9, 3}
set3 = {True, False, False}
```

A set can contain different data types:


A set with strings, integers and boolean values:
```py
set1 = {"abc", 34, True, 40, "male"}

```
---

# type()

From Python's perspective, sets are defined as objects with the data type 'set':
<class 'set'>


What is the data type of a set?
```py
myset = {"tomato", "cabbage", "cucumber"}
print(type(myset))
```

---

# The set() Constructor

It is also possible to use the set() constructor to make a set.


Using the set() constructor to make a set:
```py
thisset = set(("tomato", "cabbage", "cucumber")) # note the double round-brackets
print(thisset)
```

---

# Access Items

You cannot access items in a set by referring to an index or a key.

But you can loop through the set items using a for loop, or ask if a specified value is present in a set, by using the `in` keyword.


Loop through the set, and print the values:
```py
thisset = {"tomato", "cabbage", "cucumber"}

for x in thisset:
  print(x)


Check if "cabbage" is present in the set:
thisset = {"tomato", "cabbage", "cucumber"}

print("cabbage" in thisset)
```
---

# Change Items

Once a set is created, you cannot change its items, but you can add new items.

To add items from another set into the current set, use the update() method.


Add elements from tropical into thisset:

```py
thisset = {"tomato", "cabbage", "cucumber"}
tropical = {"zucchini", "wasabi", "papaya"}

thisset.update(tropical)

print(thisset)
```

---

# Add Any Iterable

The object in the update() method does not have to be a set, it can be any iterable object (tuples, lists, dictionaries etc.).


Add elements of a list to at set:
```py
thisset = {"tomato", "cabbage", "cucumber"}
mylist = ["potato", "lemongrass"]

thisset.update(mylist)

print(thisset) 
```

---

# Remove Item

To remove an item in a set, use the remove(), or the discard() method.


Remove "cabbage" by using the remove() method:
```py
thisset = {"tomato", "cabbage", "cucumber"}

thisset.remove("cabbage")

print(thisset)
```

> `[!]`Note: If the item to remove does not exist, remove() will raise an error.


Remove "cabbage" by using the discard() method:
```py
thisset = {"tomato", "cabbage", "cucumber"}
thisset.discard("cabbage")
print(thisset)
```

> `[!]`Note: If the item to remove does not exist, discard() will NOT raise an error.

---

# Remove Item

You can also use the pop() method to remove an item, but this method will remove the last item. Remember that sets are unordered, so you will not know what item that gets removed.

The return value of the pop() method is the removed item.


Remove the last item by using the pop() method:
```py
thisset = {"tomato", "cabbage", "cucumber"}
x = thisset.pop()
print(x)
print(thisset)
```
> `[!]`Note: Sets are unordered, so when using the pop() method, you do not know which item that gets removed.

---

# Remove Item

The clear() method empties the set:
```py
thisset = {"tomato", "cabbage", "cucumber"}

thisset.clear()

print(thisset)
```
---
# Remove Item


The del keyword will delete the set completely:
```py
thisset = {"tomato", "cabbage", "cucumber"}

del thisset

print(thisset) 
```

---
# Join Two Sets

There are several ways to join two or more sets in Python.

You can use the union() method that returns a new set containing all items from both sets, or the update() method that inserts all the items from one set into another:


The union() method returns a new set with all items from both sets:
```py
set1 = {"a", "b" , "c"}
set2 = {1, 2, 3}

set3 = set1.union(set2)
print(set3)
```

The update() method inserts the items in set2 into set1:
```py
set1 = {"a", "b" , "c"}
set2 = {1, 2, 3}

set1.update(set2)
print(set1)
```
> `[!]`Note: Both union() and update() will exclude any duplicate items.

---

# Keep ONLY the Duplicates

The intersection_update() method will keep only the items that are present in both sets.


Keep the items that exist in both set x, and set y:
```py
x = {"tomato", "cabbage", "cucumber"}
y = {"google", "microsoft", "tomato"}

x.intersection_update(y)

print(x)
```

The intersection() method will return a new set, that only contains the items that are present in both sets.


Return a set that contains the items that exist in both set x, and set y:
```py
x = {"tomato", "cabbage", "cucumber"}
y = {"google", "microsoft", "tomato"}

z = x.intersection(y)

print(z)
```
---

Keep All, But NOT the Duplicates

The symmetric_difference_update() method will keep only the elements that are NOT present in both sets.


Keep the items that are not present in both sets:
```py
x = {"tomato", "cabbage", "cucumber"}
y = {"google", "microsoft", "tomato"}

x.symmetric_difference_update(y)

print(x)
```
The symmetric_difference() method will return a new set, that contains only the elements that are NOT present in both sets.


Return a set that contains all items from both sets, except items that are present in both:
```py
x = {"tomato", "cabbage", "cucumber"}
y = {"google", "microsoft", "tomato"}

z = x.symmetric_difference(y)

print(z) 
```

---
# Dictionary

Dictionaries are used to store data values in key:value pairs.

A dictionary is a collection which is ordered*, changeable and does not allow duplicates.

As of Python version 3.7, dictionaries are ordered. In Python 3.6 and earlier, dictionaries are unordered.

Dictionaries are written with curly brackets, and have keys and values:


Create and print a dictionary:
```py
thisdict =	{
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}
print(thisdict)
```
---

# Dictionary Items

Dictionary items are ordered, changeable, and does not allow duplicates.

Dictionary items are presented in key:value pairs, and can be referred to by using the key name.


Print the "brand" value of the dictionary:
```py
thisdict =	{
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}
print(thisdict["brand"])
```

---


Ordered or Unordered?

As of Python version 3.7, dictionaries are ordered. In Python 3.6 and earlier, dictionaries are unordered.

When we say that dictionaries are ordered, it means that the items have a defined order, and that order will not change.

Unordered means that the items does not have a defined order, you cannot refer to an item by using an index.
---

# Changeable

Dictionaries are changeable, meaning that we can change, add or remove items after the dictionary has been created.

---

# Duplicates Not Allowed

Dictionaries cannot have two items with the same key:


Duplicate values will overwrite existing values:

```py
thisdict =	{
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964,
  "year": 2020
}
print(thisdict)
```

---

# Dictionary Length

To determine how many items a dictionary has, use the len() function:


Print the number of items in the dictionary:
```py
print(len(thisdict))
```
---

# Dictionary Items - Data Types

The values in dictionary items can be of any data type:


String, int, boolean, and list data types:
```py
thisdict =	{
  "brand": "Ford",
  "electric": False,
  "year": 1964,
  "colors": ["red", "white", "blue"]
}

```

---

# type()

From Python's perspective, dictionaries are defined as objects with the data type 'dict':
<class 'dict'>


Print the data type of a dictionary:
```py
thisdict =	{
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}
print(type(thisdict))
```

When choosing a collection type, it is useful to understand the properties of that type. Choosing the right type for a particular data set could mean retention of meaning, and, it could mean an increase in efficiency or security.

---
# Accessing Items

You can access the items of a dictionary by referring to its key name, inside square brackets:


Get the value of the "model" key:
```py
thisdict =	{
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}
x = thisdict["model"]
```

There is also a method called get() that will give you the same result:


Get the value of the "model" key:
```py
x = thisdict.get("model")
```

---
# Get Keys

The keys() method will return a list of all the keys in the dictionary.


Get a list of the keys:
```py

x = thisdict.keys()
```

The list of the keys is a view of the dictionary, meaning that any changes done to the dictionary will be reflected in the keys list.


Add a new item to the original dictionary, and see that the keys list gets updated as well:
```py
car = {
"brand": "Ford",
"model": "Mustang",
"year": 1964
}

x = car.keys()

print(x) #before the change

car["color"] = "white"

print(x) #after the change

```

---
# Get Values

The values() method will return a list of all the values in the dictionary.


Get a list of the values:
```py
x = thisdict.values()
```

The list of the values is a view of the dictionary, meaning that any changes done to the dictionary will be reflected in the values list.


Make a change in the original dictionary, and see that the values list gets updated as well:
```py
car = {
"brand": "Ford",
"model": "Mustang",
"year": 1964
}

x = car.values()

print(x) #before the change

car["year"] = 2020

print(x) #after the change
```

Add a new item to the original dictionary, and see that the values list gets updated as well:
```py
car = {
"brand": "Ford",
"model": "Mustang",
"year": 1964
}

x = car.values()

print(x) #before the change

car["color"] = "red"

print(x) #after the change
```
---


# Get Items

The items() method will return each item in a dictionary, as tuples in a list.


Get a list of the key:value pairs
```py
x = thisdict.items()
```

The returned list is a view of the items of the dictionary, meaning that any changes done to the dictionary will be reflected in the items list.


Make a change in the original dictionary, and see that the items list gets updated as well:
```py
car = {
"brand": "Ford",
"model": "Mustang",
"year": 1964
}

x = car.items()

print(x) #before the change

car["year"] = 2020

print(x) #after the change
```

---

Add a new item to the original dictionary, and see that the items list gets updated as well:
```py
car = {
"brand": "Ford",
"model": "Mustang",
"year": 1964
}

x = car.items()

print(x) #before the change

car["color"] = "red"

print(x) #after the change
```

---

# Check if Key Exists

To determine if a specified key is present in a dictionary use the in keyword:


Check if "model" is present in the dictionary:
```py
thisdict =	{
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}
if "model" in thisdict:
  print("Yes, 'model' is one of the keys in the thisdict dictionary") 
```

---

# Change Values

You can change the value of a specific item by referring to its key name:


Change the "year" to 2018:
```py
thisdict =	{
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}
thisdict["year"] = 2018
```

---

# Update Dictionary

The update() method will update the dictionary with the items from the given argument.

The argument must be a dictionary, or an iterable object with key:value pairs.


Update the "year" of the car by using the update() method:
```py
thisdict =	{
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}
thisdict.update({"year": 2020}) 
```

---

# Adding Items

Adding an item to the dictionary is done by using a new index key and assigning a value to it:

```py
thisdict =	{
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}
thisdict["color"] = "red"
print(thisdict)
```

---

# Update Dictionary

The update() method will update the dictionary with the items from a given argument. If the item does not exist, the item will be added.

The argument must be a dictionary, or an iterable object with key:value pairs.


Add a color item to the dictionary by using the update() method:
```py
thisdict =	{
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}
thisdict.update({"color": "red"}) 
```

---

# Removing Items

There are several methods to remove items from a dictionary:


The pop() method removes the item with the specified key name:
```py
thisdict =	{
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}
thisdict.pop("model")
print(thisdict)
```
---

The popitem() method removes the last inserted item (in versions before 3.7, a random item is removed instead):

```py
thisdict =	{
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}
thisdict.popitem()
print(thisdict)
```
---

The del keyword removes the item with the specified key name:
```py
thisdict =	{
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}
del thisdict["model"]
print(thisdict)
```
---


The del keyword can also delete the dictionary completely:
```py
thisdict =	{
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}
del thisdict
print(thisdict) #this will cause an error because "thisdict" no longer exists.
```

---


The clear() method empties the dictionary:
```py
thisdict =	{
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}
thisdict.clear()
print(thisdict)
```
---

# Copy a Dictionary

You cannot copy a dictionary simply by typing dict2 = dict1, because: dict2 will only be a reference to dict1, and changes made in dict1 will automatically also be made in dict2.

There are ways to make a copy, one way is to use the built-in Dictionary method copy().


Make a copy of a dictionary with the copy() method:
```py
thisdict =	{
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}
mydict = thisdict.copy()
print(mydict)
```
Another way to make a copy is to use the built-in function dict().


Make a copy of a dictionary with the dict() function:
```py
thisdict =	{
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}
mydict = dict(thisdict)
print(mydict)
```

---
# Nested Dictionaries

A dictionary can contain dictionaries, this is called nested dictionaries.

Create a dictionary that contain three dictionaries:
```py
myfamily = {
  "child1" : {
    "name" : "Emil",
    "year" : 2004
  },
  "child2" : {
    "name" : "Tobias",
    "year" : 2007
  },
  "child3" : {
    "name" : "Linus",
    "year" : 2011
  }
}
```

--- 
 if you want to add three dictionaries into a new dictionary:


Create three dictionaries, then create one dictionary that will contain the other three dictionaries:
```py
child1 = {
  "name" : "Emil",
  "year" : 2004
}
child2 = {
  "name" : "Tobias",
  "year" : 2007
}
child3 = {
  "name" : "Linus",
  "year" : 2011
}

myfamily = {
  "child1" : child1,
  "child2" : child2,
  "child3" : child3
} 
```

---

