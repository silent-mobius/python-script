
---

# Exception handling: Try.. Except

- The try block lets you test a block of code for errors.
- The except block lets you handle the error.
- The finally block lets you execute code, regardless of the result of the try- and except blocks.

---

# Exception Handling

When an error occurs, or exception as we call it, Python will normally stop and generate an error message.
These exceptions can be handled using the try statement:

The try block will generate an exception, because x is not defined:
```py
try:
  print(x)
except:
  print("An exception occurred")
```

---

Since the try block raises an error, the except block will be executed.
Without the try block, the program will crash and raise an error:
This statement will raise an error, because x is not defined:
```py
print(x)
```

---

# Practice

create python script that :
  - uses variable that has not been defined with value
  - uses  `except` statement to evade exit and asks for value for that variable.

---

# Many Exceptions

You can define as many exception blocks as you want, e.g. if you want to execute a special block of code for a special kind of error:


Print one message if the try block raises a NameError and another for other errors:
```py
try:
  print(x)
except NameError:
  print("Variable x is not defined")
except:
  print("Something else went wrong")
```

---
# Practice

create python script that :
  - import `sys` library
  - set variable to keep in value of argument.
  - uses variable that has not been defined with value
  - uses  `except` statement to evade exit and asks for value for that variable.
  - uses another `except` statement in case the the value of variable is not convertible to int


---

# Else

You can use the else keyword to define a block of code to be executed if no errors were raised:

In this , the try block does not generate any error:
```py
try:
  print("Hello")
except:
  print("Something went wrong")
else:
  print("Nothing went wrong")
```

---
# Practice

- create python script that has a function:
  - import `ipaddress` library
  - requires ipv4 without cidr from user.
  - in case cidr is added, print error message.
  - in case of ip address in not valid, print error
<!-- import ipaddress
def validate_ip_address(ip_string):
   try:
       ip_object = ipaddress.ip_address(ip_string)
       print("The IP address '{ip_object}' is valid.")
   except ValueError:
       print("The IP address '{ip_string}' is not valid")

validate_ip_address("127.0.0.1") -->
---
# Finally

The finally block, if specified, will be executed regardless if the try block raises an error or not.
```py
try:
  print(x)
except:
  print("Something went wrong")
finally:
  print("The 'try except' is finished")
```
This can be useful to close objects and clean up resources:


Try to open and write to a file that is not writable:
```py
try:
  f = open("demofile.txt")
  try:
    f.write("Lorum Ipsum")
  except:
    print("Something went wrong when writing to the file")
  finally:
    f.close()
except:
  print("Something went wrong when opening the file")
```
The program can continue, without leaving the file object open.

---
# Practice

- create python script:
  - import `Path` from `pathlib` library
  - import `sys` library
  - pass value from `sys.argv[1]` to variable
  - use `exists()` to check evaluation whether path exists`. 

---

# Raise an exception

As a Python developer you can choose to throw an exception if a condition occurs.

To throw (or raise) an exception, use the raise keyword.
Raise an error and stop the program if x is lower than 0:
```py
x = -1

if x < 0:
  raise Exception("Sorry, no numbers below zero")
```
The `raise` keyword is used to raise an exception.

---

You can define what kind of error to raise, and the text to print to the user.


Raise a TypeError if x is not an integer:

```py
x = "ip-address"

if not type(x) is int:
  raise TypeError("Only integers are allowed") 
```

---
# Practice : Error handling

- create python script that will list the content of current folder:
  - in case there is no `.txt` extension files, raise error


---
# File Handling

File handling is an important part of any programming language.

Python has several functions for creating, reading, updating, and deleting files.

In this module we'll go over the capabilietes of Python to handle files in various ways, by reading, writing, parsing data saved in files.

---

# File Handling

The key function for working with files in Python is the open() function.

The `open()` function takes two parameters; filename, and mode.

There are four different methods (modes) for opening a file:

- "r" | Read - Default value. Opens a file for reading, error if the file does not exist
- "r+"| Read and Write - Opens file and allows to write in to it
    - In case file does not exists, will get error
- "a" | Append - Opens a file for appending only, creates the file if it does not exist
- "a+"| Read and Append - Opens files for reading and apprending to the file.
- "w" | Write - Opens a file for writing:
    - In case file does __not exist__, it will be __created__
    - In case file __exists__, the data will be __squahsed__
- "w+"| Read and Write - Opens file and allows to write in to it
    - In case file does __not exist__, it will be __created__
    - In case file __exists__, the data will be __squahsed__ 
- "x" | Create - Creates the specified file, returns an error if the file exists

---
# File Habdling Table 

| Mode             | r  | r+ | w  | w+ | a  | a+ |
| ---------------- | -- | -- | -- | -- | -- | -- |
| read             | +  | +  |    |  + |    |  + |
| Write            |    | +  | +  |  + |  + |  + |
| Write after seek |    | +  | +  |  + |    |    |
| Create           |    |    | +  |  + |  + |  + |
| Truncate         |    |    | +  |  + |    |    |
| Position at start| +  | +  | +  |  + |    |    |
| Position at end  |    |    |    |    |  + |  + |

--- 

In addition you can specify if the file should be handled as binary or text mode

- "t" - Text - Default value. Text mode
- "b" - Binary - Binary mode (e.g. images)

---

# Syntax

To open a file for reading it is enough to specify the name of the file:
```py
f = open("demofile.txt")
```
The code above is the same as:
```py
f = open("demofile.txt", "rt")
```
Because "r" for read, and "t" for text are the default values, you do not need to specify them.

> `[!]`Note: Make sure the file exists, or else you will get an error.

---

# Open a File on the Server

Assume we have the following file, located in the same folder as Python:

```sh
alex@vaiolabs:~$ cat demofile.txt
Hello! Welcome to demofile.txt
This file is for testing purposes.
Good Luck!
```
To open the file, use the built-in `open()` function.

The open() function returns a file object, which has a read() method for reading the content of the file:


```py
f = open("sh_ip_int.txt", "r")
print(f.read())
```
---

If the file is located in a different location, you will have to specify the file path, like this:

Open a file on a different location:
```py
f = open("/home/aschapelle/Destop/sh_ip_int.txt", "r")
print(f.read())
```
---

# Read Only Parts of the File

By default the read() method returns the whole text, but you can also specify how many characters you want to return:


Return the 5 first characters of the file:
```py
f = open("demofile.txt", "r")
print(f.read(5))
```

---
# Read Lines

You can return one line by using the readline() method:

Read one line of the file:
```py
f = open("demofile.txt", "r")
print(f.readline())
```
By calling readline() two times, you can read the two first lines:


Read two lines of the file:
```py
f = open("demofile.txt", "r")
print(f.readline())
print(f.readline())
```

---

By looping through the lines of the file, you can read the whole file, line by line:


Loop through the file line by line:
```py
f = open("demofile.txt", "r")
for x in f:
  print(x)
```

---

# Close Files

It is a good practice to always close the file when you are done with it.


Close the file when you are finish with it:
```py
f = open("demofile.txt", "r")
print(f.readline())
f.close()
```
> `[!]`Note: You should always close your files, in some cases, due to buffering, changes made to a file may not show until you close the file.

---

# Write to an Existing File

To write to an existing file, you must add a parameter to the open() function:

- "a" | Append - will append to the end of the file
- "w" | Write - will overwrite any existing content


Open the file "demofile2.txt" and append content to the file:
```py
f = open("demofile2.txt", "a")
f.write("Now the file has more content!")
f.close()
```

---

#open and read the file after the appending:
```py
f = open("demofile2.txt", "r")
print(f.read())
```

Open the file "demofile3.txt" and overwrite the content:
```py
f = open("demofile3.txt", "w")
f.write("Woops! I have deleted the content!")
f.close()

#open and read the file after the appending:
f = open("demofile3.txt", "r")
print(f.read())
```

> `[!]`Note: the "w" method will overwrite the entire file.

---

# Create a New File

To create a new file in Python, use the open() method, with one of the following parameters:

- "x" | Create - will create a file, returns an error if the file exist
- "a" | Append - will create a file if the specified file does not exist
- "w" | Write - will create a file if the specified file does not exist


Create a file called "myfile.txt":
```py
f = open("myfile.txt", "x")
```
Result: a new empty file is created!


Create a new file if it does not exist:
```py
f = open("myfile.txt", "w")
```

---
# Delete a File

To delete a file, you must import the OS module, and run its os.remove() function:


Remove the file "demofile.txt":
```py
import os
os.remove("demofile.txt")
```
Check if File exist:

To avoid getting an error, you might want to check if the file exists before you try to delete it:

Check if file exists, then delete it:
```py
import os
if os.path.exists("demofile.txt"):
  os.remove("demofile.txt")
else:
  print("The file does not exist")
```

---

# Delete Folder

To delete an entire folder, use the os.rmdir() method:

Remove the folder "myfolder":
```py
import os
os.rmdir("myfolder")
```
> `[!]`Note: You can only remove empty folders.
